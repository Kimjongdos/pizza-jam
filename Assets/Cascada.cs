﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cascada : MonoBehaviour
{
   public float Multiplicar;
   private float offsetX = -11f;

   void Start(){
       transform.position = new Vector3(transform.position.x,transform.position.y,-1);
       Multiplicar = 1.3f;
   }

    void Update()
    {
        offsetX = offsetX + Time.deltaTime * Multiplicar;
       transform.position = new Vector3(transform.position.x,offsetX,-1);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "VelocidadAgua"){
            Multiplicar+= 0.5f;
        } 
    }
}
