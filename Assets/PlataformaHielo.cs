﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaHielo : MonoBehaviour
{
    public Animator anim;


    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player"){
            anim.SetTrigger("Derretir");
        }
    }
    public void destroy(){
        Destroy(gameObject);
    }

   
}
