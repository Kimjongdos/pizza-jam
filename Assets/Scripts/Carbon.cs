﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carbon : MonoBehaviour
{
    public GameObject fx;
    private Weapon weapon;

    public AudioSource PickSound;

    void Awake(){
        weapon = GameObject.Find("Weapon").GetComponent<Weapon>();
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player"){
            if(weapon.municion<=3){
            PickSound.Play();
            Instantiate(fx,transform.position,Quaternion.identity);
            Destroy(gameObject,0.01f);

            }
        }
        
       
    }
}
