﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour{
public float visionRadius,attackRadius,speed;
 public GameObject fxDie;
 GameObject player;
 Vector3 initialPosition;
 Rigidbody2D rb2d;

 public SpriteRenderer sprite;
 public bool Flip = false;
 public GameObject Bullet;
 public float TimeToShoot;
 private float counter;

 public Transform bulletTransform;

 public int Lifes;

 private Vector3 reduction;

 public AudioSource Hit;
 
 void Start () {
  
  player = GameObject.FindGameObjectWithTag("Player");
  initialPosition = transform.position;
  rb2d = GetComponent<Rigidbody2D>();
  
    reduction = new Vector3(0.2876837f,0.2876837f,0.2876837f);
    transform.localScale = reduction;
 }
 
 
    void Update () {
        IA();
        Life();
        counter+= Time.deltaTime;

    }

    public void Life(){
       
        if(Lifes == 1){
            transform.localScale = new Vector3(0.1876837f,0.1876837f,0.1876837f);
        }
        if(Lifes==0){
            Instantiate(fxDie,transform.position,Quaternion.identity,null);
            Destroy(gameObject,0.01f);

        }
                
            

        
    }



    public void IA(){
        Vector3 target = initialPosition;
        RaycastHit2D hit = Physics2D.Raycast(transform.position,player.transform.position - transform.position, visionRadius , 1 << LayerMask.NameToLayer("Default"));

        //debug
        Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
        Debug.DrawRay (transform.position, forward, Color.red);

        //si encuentra al player
        if (hit.collider != null) {
            if (hit.collider.tag == "Player") {
                target = player.transform.position;
            }
        }

        float distance = Vector3.Distance(target, transform.position);
        Vector3 dir = (target - transform.position).normalized;

        //Rango de ataque
        if (target != initialPosition && distance < attackRadius) {
            if(Random.Range(2f,4f)<counter){
                Instantiate(Bullet,bulletTransform.transform.position,transform.rotation);
                counter = 0;
            }
        }
        else {
        rb2d.MovePosition(new Vector2(transform.position.x + dir.x * speed * Time.deltaTime,rb2d.position.y));
        //Se mueve
        }
        if (target == initialPosition && distance < 0.02f) {
        transform.position = initialPosition;
        //idle 
        }

        Debug.DrawLine(transform.position, target, Color.green);


        if(Flip && dir.x<0f || !Flip && dir.x>0f) {
            FlipX();
        }
        
    }
    void OnDrawGizmosSelected() {
    Gizmos.color = Color.yellow;
    Gizmos.DrawWireSphere(transform.position, visionRadius);
    Gizmos.DrawWireSphere(transform.position, attackRadius);
    }
    public void FlipX(){
        
        Flip = !Flip;

        transform.Rotate(0,180,0);
        
    
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Bullet"){
            Lifes--;
            Hit.Play();

        }
    }
    

    
}
