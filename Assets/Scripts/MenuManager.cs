﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
public AudioSource click;
    
    void Start(){
        Cursor.visible = true;
    }
    public void PulsaStart(){
            click.Play(); 
            SceneManager.LoadScene("GamePlayScreen");
    }
     public void PulsaMenu(){
            click.Play(); 
            SceneManager.LoadScene("MenuScreen");
    }

    public void PulsaExit(){
        Application.Quit();
    }

}