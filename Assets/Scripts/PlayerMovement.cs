﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public Animator animTermo;
    public Weapon weapon;
    public float speed;
    public float H;

    private Rigidbody2D rb2d;

    public float VelocidadMax;

    public bool Jump;
    public bool Grounded;

    public float FuerzaDeSalto = 10f;

    public bool Flip = true;

    public SpriteRenderer sprite;

    public Animator anim;

    public Image Mercurio;
    public GameObject tm_caliente,tm_normal,tm_frio;
    public float life = 0.8f;
    public float TiempoVida;

    public AudioSource hit;

    public AudioSource WalkingSound;
    public bool Walking;

    public GameObject FXdie;

    public AudioSource Vapor;
    public LayerMask whatIsGround;
    public Transform feet;

    void Awake(){
        rb2d = GetComponent<Rigidbody2D>();
        Mercurio.fillAmount = life;
        Cursor.visible = false;
        
    }

    void Update(){
        LifeManager();
        H = Input.GetAxis("Horizontal");
        Walking = Input.GetButton("Horizontal");

        if(Walking && Grounded){
            if(!WalkingSound.isPlaying){
                WalkingSound.Play();
            }
            
        }else if(Walking == false){
            WalkingSound.Stop();
        }
        Grounded = Physics2D.OverlapCircle(feet.position,0.3f,whatIsGround);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       
        Movimiento();
        
        if(H > 0.1 && Flip || H < 0 && !Flip){

            FlipX();

        }
        InstanciaVida();
         
    }


    public void Movimiento(){
       
        
        rb2d.AddForce(Vector2.right * H * speed * 2);
        //Limitar velocidad al player
        float SpeedMax = Mathf.Clamp(rb2d.velocity.x,-VelocidadMax,VelocidadMax);
        rb2d.velocity = new Vector2(SpeedMax,rb2d.velocity.y);

        if(Input.GetKeyDown(KeyCode.Space) && Grounded){

            Jump = true;
        }

        if(Jump){

           
            rb2d.velocity = Vector2.up * FuerzaDeSalto;
            Jump = false;
        }
        //Animaciones
        float ABS = Mathf.Abs(rb2d.velocity.x);
        anim.SetFloat("velocity",ABS);

    }
    public void FlipX(){
        
        Flip = !Flip;

        transform.Rotate(0,180,0);    
    }
   
    
    public void LifeManager(){
        Mercurio.fillAmount = life;
        life = Mathf.Clamp(life,0,1);
        life -= Time.deltaTime/TiempoVida;

        if(life>0.8f){ //Temperatura fria
            tm_caliente.SetActive(true);
            tm_normal.SetActive(false);
            tm_frio.SetActive(false);
        }
        if(life<= 0.8f && life>0.45f){ //Temperatura Normal
            tm_caliente.SetActive(false);
            tm_normal.SetActive(true);
            tm_frio.SetActive(false);
        }
        if(life<=0.45f){
            tm_caliente.SetActive(false);
            tm_normal.SetActive(false);
            tm_frio.SetActive(true); 
        }
        
        
    }
    public void InstanciaVida(){
        if(Mercurio.fillAmount <= 0 || life <=0){//muerte por el termometro o por los pinchos
            
            //StartCoroutine(muerte());
            StartCoroutine(muerte());
          
            Debug.Log("estas to muerto");
            

        }

        

    }
   

    private void OnParticleCollision(GameObject other)
    {
        if(other.gameObject.tag == "Particulas"){
            Vapor.Play();  
            life -= 0.050f;

        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Carbon"){
            if(weapon.municion<3){
                weapon.municion += 1;
                life += 0.2f;
            }

           
        }

        if(other.tag =="BulletEnemy"){
            
            StartCoroutine(Parpadeo());
            
            InstanciaVida();//muerte
            
        }

        if(other.tag == "Cascada"){
            
            StartCoroutine(muerteCascada());
           
        }

        if(other.tag == "YouWin"){
            StartCoroutine(YouWIn());
        }
    }

    IEnumerator Parpadeo(){
       
            hit.Play();
            animTermo.SetTrigger("HIT");
            life-= 0.25f;
            sprite.enabled = false;
            yield return new WaitForSeconds(0.25f);
            sprite.enabled = true;
            
       
        
        
    }

    IEnumerator muerte(){
        Mercurio.fillAmount = 0;
        //sprite.enabled = false;
        Instantiate(FXdie,transform.position,Quaternion.identity,null);
            
       
        
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("GameOverScreen");
        Destroy(gameObject);

       

    }
     IEnumerator muerteCascada(){
        Mercurio.fillAmount = 0;
        
        Instantiate(FXdie,transform.position,Quaternion.identity,null);
        sprite.enabled = false;
        Destroy(gameObject,0.05f);
        SceneManager.LoadScene("GameOverScreen");
        yield return new WaitForSeconds(0.1f);
        
        

       

    }
    IEnumerator YouWIn(){
    yield return new WaitForSeconds(3f);
    SceneManager.LoadScene("YouWinScreen");    
        

    }





}
