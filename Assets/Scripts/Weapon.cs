﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject bullet;
    public GameObject FXShoot;
    public GameObject SpriteAtaque;
    public AudioSource canNotShoot;

    public GameObject Carbon1,Carbon2,Carbon3;

    public AudioSource SoundShoot;

    public int municion = 0;
   
    

    // Update is called once per frame
    void Update()
    {
        if(municion>0){
             
             if(Input.GetKeyDown(KeyCode.X)){
                SpriteAtaque.SetActive(true);
                Shoot(); 
            }
            if(Input.GetKeyUp(KeyCode.X)){
                SpriteAtaque.SetActive(false);
            }
        }

        if(Input.GetKeyDown(KeyCode.X) && municion == 0){
            canNotShoot.Play();
            SpriteAtaque.SetActive(false);
        }

        switch (municion){
            case 0:
                Carbon1.SetActive(false);
                Carbon2.SetActive(false);
                Carbon3.SetActive(false);
                break;
            case 1:
                Carbon1.SetActive(true);
                Carbon2.SetActive(false);
                Carbon3.SetActive(false);
                break;
            case 2:
                Carbon1.SetActive(true);
                Carbon2.SetActive(true);
                Carbon3.SetActive(false);
                break;
            case 3:
                Carbon1.SetActive(true);
                Carbon2.SetActive(true);
                Carbon3.SetActive(true);
                break;

        }
          

           

    }

    public void Shoot(){
        municion--;
        
        Instantiate(FXShoot,transform.position,transform.rotation);
        SoundShoot.Play();
        Instantiate(bullet,transform.position,transform.rotation);
       
    }
}
