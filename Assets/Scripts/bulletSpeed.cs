﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletSpeed : MonoBehaviour
{
    
    private PlayerMovement player;
    public Rigidbody2D rb2d;
    public float speed = 20f;
    
    private SpriteRenderer sprite;


    void Awake(){
    rb2d.velocity = transform.right * speed * 100 * Time.deltaTime;
    sprite = GetComponent<SpriteRenderer>();
    player = GameObject.Find("Player").GetComponent<PlayerMovement>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Enemy"){
            player.life += 0.15f;
            sprite.enabled = false;
            rb2d.velocity = new Vector2(0,0);
            Destroy(this.gameObject,5f);

        }
    }

    
    
    
}
