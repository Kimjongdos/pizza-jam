﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletSpeedEnemy : MonoBehaviour
{
    
    public Rigidbody2D rb2d;
    public float speed = 20f;
    
    private SpriteRenderer sprite;


    void Awake(){
    rb2d.velocity = transform.right * speed * 100 * Time.deltaTime;
    sprite = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player"){
            sprite.enabled = false;
            rb2d.velocity = new Vector2(0,0);
            Destroy(this.gameObject,5f);

        }
    }

    
    
    
}
